import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

const AppRouter = () => {

    return (
        <Router>
            <Switch>
                <Route path='/main' exact>Hello world</Route>


                {/*if no match*/}
                <Route path="/">
                    <Redirect to="/main"/>
                </Route>
            </Switch>
        </Router>
    )
};

export default AppRouter;